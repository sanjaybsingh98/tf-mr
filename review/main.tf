provider "aws" {
  region     = var.region
  access_key = var.access_key
  secret_key = var.secret_key
  version    = "~> 2.0"
}

module "ec2_module" {
  source = "../modules"

  vmname                   = var.vmname
  access_key               = var.access_key
  secret_key               = var.secret_key
  ssh_key_name             = var.ssh_key_name
  vpc_cidr_block           = var.vpc_cidr_block
  dmz_cidr_block           = var.dmz_cidr_block
  main_instance_private_ip = var.main_instance_private_ip
}
