# Terraform Example

### Project variables

This project needs to have three project variables defined. They are:

* `GTILAB_API_TOKEN` - Created via GitLab UI (User Settings -> Access Tokens -> Create personal access token). Ensure the token scope is "api"
* `TF_VAR_access_key` - `AWS access key` found in your local directory $HOME/.aws/credentials (to set up, please refer to AWS documentation)
* `TF_VAR_secret_key` - `AWS secret key` found in your local directory $HOME/.aws/credentials (to set up, please refer to AWS documentation)

### Directory structure

* `modules` - Terraform re-usable modules
  * `main.tf` - Terraform module for the EC2 Ubuntu virtual machine
  * `variables.tf` - Terraform module input variables declaration
  * `vpc.tf` - Terraform module for the VPC
* `production` - directory for Terraform files specific to production env
  * `backend.tf` - GitLab managed Terraform HTTP state backend
  * `main.tf` - top-level prod Terraform configuration file. It reuses Terraform files in modules dir and passes to it production-specific values
  * `terraform.tfvars` - variable definitions specific to production
  * `variables.tf` - Terraform variable declarations
* `review` - directory for Terraform files specific to review env
  * `backend.tf` - GitLab managed Terraform HTTP state backend
  * `main.tf` - top-level prod Terraform configuration file. It reuses Terraform files in modules dir and passes to it review-specific values
  * `terraform.tfvars` - variable definitions specific to review
  * `variables.tf` - Terraform variable declarations
* `staging` - directory for Terraform files specific to staging env
  * `backend.tf` - GitLab managed Terraform HTTP state backend
  * `main.tf` - top-level prod Terraform configuration file. It reuses Terraform files in modules dir and passes to it staging-specific values
  * `terraform.tfvars` - variable definitions specific to staging
  * `variables.tf` - Terraform variable declarations
* `.gitlab-ci.yml` - main CI configuration. It runs prepare, validate, build and manual deploy for all environments
